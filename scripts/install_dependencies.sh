#!/bin/bash
# File: scripts/install_dependencies.sh
# Installs the dependencies using `npm install`
source ~/.bash_profile
cd /var/acebook
sudo yum install -y npm
wget -N http://mirror.centos.org/centos/7/os/x86_64/Packages/openssl-libs-1.0.2k-19.el7.x86_64.rpm
sudo rpm -ivh openssl-libs-1.0.2k-19.el7.x86_64.rpm --force
echo -e "[mongodb-org-6.0]\nname=MongoDBRepository\nbaseurl=https://repo.mongodb.org/yum/amazon/2/mongodb-org/6.0/x86_64/\ngpgcheck=1\nenabled=1\ngpgkey=https://www.mongodb.org/static/pgp/server-6.0.asc" | sudo tee -i /etc/yum.repos.d/mongodb-org-6.0.repo
sudo yum install -y mongocli mongodb-mongosh
wget -N https://truststore.pki.rds.amazonaws.com/global/global-bundle.pem
npm install

