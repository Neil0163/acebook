#!/bin/bash
# File: scripts/setup_mongod.sh
# Sets up mongod database 
# grants execute permission to ec2 user 
sudo yum update -y
sudo yum install -y mongodb-org
# Create /data/db directory
sudo mkdir -p /data/db
sudo chown -R $USER /data/db
# Start MongoDB
mongod --dbpath /data/db  --bind_ip_all 
# Wait for MongoDB to start (you may need to adjust the sleep duration)
sleep 10
# Retrieve MongoDB credentials from Secrets Manager
MONGODB_CREDENTIALS=$(aws secretsmanager get-secret-value --secret-id NeilDocDbSecret)
echo "MongoDB Credentials: $MONGODB_CREDENTIALS"
# Extract MongoDB connection details
MONGO_HOST=$(echo $MONGODB_CREDENTIALS | jq -r '.SecretString | fromjson | .host')
MONGO_USER=$(echo $MONGODB_CREDENTIALS | jq -r '.SecretString | fromjson | .username')
MONGO_PASSWORD=$(echo $MONGODB_CREDENTIALS | jq -r '.SecretString | fromjson | .password')
MONGO_AUTH_DB=$(echo $MONGODB_CREDENTIALS | jq -r '.SecretString | fromjson | .authenticationDatabase')
